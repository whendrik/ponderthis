use std::thread;
use std::sync::{Arc, Mutex};

//
// Method used:
//
// - Only test a selected few bot placements, found by statistical analysis  
// - Warp every few rounds to speed-up walking along 2's
// - multi threads for speedup


//
// B1, i : **only** along edge, i.e. x = 0
//
// B2, j : **only** if;
//		- on edge
//		- on same row, i.e. B1 y == B2 y 
//		- in lower-right of the grid, that is; (x,y) both in last 75%
//

fn main() {
	println!("Lets Find Some Coverage!");
	

	for n in 10..3000 {
		multi_thread_find(n,3);
		println!();
	}
}

fn multi_thread_find( n: i32, threads: i32) {
	let counter = Arc::new(Mutex::new(0));
	let mut handles = vec![];

	for chunk in 0..threads {
		let counter = Arc::clone(&counter);
		let handle = thread::spawn(move || {

			find_block_placement(n as usize, threads, chunk, counter);
		});
		handles.push(handle);
	}

	//
	// Needed to make sure each thread is finished before program termination
	//
	for handle in handles {
		handle.join().unwrap();
	}

}

fn step(grid: &mut [u8], position: &mut usize, face: &mut u8, vaccinated : &mut u32, n : usize, warp_left: & [i32], warp_right: & [i32], warp_up: & [i32], warp_down: & [i32], warp_active : bool) {
	let current_vaccinated: u8 = grid[*position];

	if current_vaccinated == 2 {
		//
		// If warp is active, lets warp and move quickly through the grid along 2's
		//
		
		if warp_active {
			if *face == 1 {
				let x = *position % n;
				let y = *position / n;

				if (x as i32) < warp_right[y] || ( (x as i32) > warp_left[y] && warp_right[y] >= 0) {
					*position = n*y + (warp_right[y] as usize);
				} 
			}
			if *face == 3 {
				let x = *position % n;
				let y = *position / n;

				if (x as i32) > warp_left[y] || ((x as i32) < warp_right[y] && warp_left[y] < n as i32) {
					*position = n*y + (warp_left[y] as usize);
				} 
			}
			if *face == 0 {
				let x = *position % n;
				let y = *position / n;

				if (y as i32) > warp_up[x] || ((y as i32) < warp_down[x] && warp_up[x] < (n as i32)) {
					*position = n*(warp_up[x] as usize) + x;
				}
			}
			if *face == 2 {
				let x = *position % n;
				let y = *position / n;

				if (y as i32) < warp_down[x] || ((y as i32) > warp_up[x] && warp_down[x] >= 0 ) {
					*position = n*(warp_down[x] as usize) + x;
				}
			}
		}


	} else if current_vaccinated == 0 {
		*face = (*face + 1) & 3;
		grid[*position] = 1;
		*vaccinated += 1;
	}
	else if current_vaccinated == 1 {
		*face = (*face + 3) & 3;
		grid[*position] = 2;
		*vaccinated += 1;
	}
	else if current_vaccinated == 3 || current_vaccinated == 4 {
		*face = (*face + 3) & 3;
		grid[*position] = 4; // We want to remember we have been here..		
	}

	if *face == 1 {
		//
		// ->
		//

		if *position % n < n - 1 {
			*position += 1;
		}
		else {
			*position -= n - 1;
		}
	}
	else if *face == 3 {
		//
		// <-
		//

		if *position % n > 0 {
			*position -= 1;
		}
		else {
			*position += n-1;
		}
	}
	else if *face == 0 {
		//
		// ^
		//

		if *position / n > 0 {
			*position -= n;
		}
		else {
			*position += n * (n-1);
		}
	}
	else if *face == 2 {
		//
		// v
		//

		if *position / n < n - 1 {
			*position += n;
		}
		else {
			*position -= n * (n-1);
		}
	}

}

fn _print_grid( grid: & [u8], n : usize) {
	let mut i = 0;

	for vac in grid.iter() {
		if i % n == 0 {
			print!("\n");
		}
		if *vac == 0 as u8 {
			print!(".");
		}
		if *vac == 1 as u8 {
			print!(":");
		}
		if *vac == 2 as u8 {
			print!("-");
		}
		if *vac == 3 || *vac == 4 as u8 {
			print!("B");
		}
		i = i + 1;
	}
	print!("\n");
}

fn find_block_placement(n : usize, chunks: i32, chunk: i32, counter: Arc<Mutex<i32>>) -> i32 {
	let mut grid : Vec<u8>;
	let mut position: usize;
	let mut face : u8;

	let mut vaccinated : u32;
	let mut last_vaccinated : i32;

	println!("Try to find a solution for n = {}...",n);

	grid = vec![0; n * n];

	let mut warp_left : Vec<i32> = vec![n as i32; n];
	let mut warp_right : Vec<i32> = vec![-1; n];
	let mut warp_up : Vec<i32> = vec![n as i32; n];
	let mut warp_down : Vec<i32> = vec![-1; n];

	let mut max_vacs = 0;
	let mut max_i = 0;
	
	let mut vacs_vec : Vec<i32> = vec![];
	let mut i_vec : Vec<i32> = vec![];

	for i in 0..(n*n) as i32 {

		if i % chunks == chunk {

			let mut allowed_to_skip_j = false;
			let mut skippable_j : Vec<i32> = Vec::new();

			for j in i..(n*n) as i32{

				if allowed_to_skip_j && skippable_j.contains(&j) {
					let _x = grid[ j as usize];
				}
				else {
					grid = vec![0; n * n];

					grid[ i as usize ] = 3;
					grid[ j as usize ] = 3;

					face = 0;
					position = 0;
					vaccinated = 0;
					last_vaccinated = -1;

					warp_left = vec![n as i32; n];
					warp_right = vec![-1; n];
					warp_up = vec![n as i32; n];
					warp_down = vec![-1; n];
						
					let mut rounds = 0;
					let mut warp_active : bool = false;
					
					while last_vaccinated != vaccinated as i32 {
						last_vaccinated = vaccinated as i32 ;

						for _k in 0..10*n {
							step(&mut grid, &mut position, &mut face, &mut vaccinated, n, & warp_left, & warp_right, & warp_up, & warp_down, warp_active);
						}
						rounds += 1;

						//
						// Create Warp Holes every few rounds.
						// To Speed up for larger N (N > 300)
						//
						// For N < 500, the speedup is minor
						// For N > 500, the speedup becomes visible
						//
						// The cost of calculating warp-holes is
						// neglectable if done only every so rounds
						//
						// The administration of using warp-holes is
						// neglectable a we *only* use them if active
						//
						if (rounds + 1) % 20 == 0 {
							warp_active = true;
							for y in 0..n {
								let mut x = 0;
								while grid[n*y + x] == 2 && x < (n-1) {
									x += 1;
								}
								warp_right[y] = (x - 1) as i32;

								x = n-1;
								while grid[n*y + x] == 2 && x > 0 {
									x -= 1;
								}
								warp_left[y] = (x + 1) as i32;
							}

							for x in 0..n {
								let mut y = 0;
								while grid[n*y + x] == 2 && y < (n-1) {
									y += 1;
								}
								warp_down[x] = (y - 1) as i32;

								y = n-1;

								while grid[n*y + x] == 2 && y > 0 {
									y -= 1;
								}
								warp_up[x] = (y + 1) as i32;
							}
						}
					}


					{
						if grid[ j as usize ] == 3 {
							allowed_to_skip_j = true;
							for possible_j_to_skip in j..(n*n) as i32 {
								if grid[ possible_j_to_skip as usize] == 0 {
									skippable_j.push( possible_j_to_skip );
								}
							}
							// println!{ "i: {}", i	}
							// println!{ "j: {}", j	}
							// _print_grid( &grid, n );
						}

					}

					if vaccinated + 4 >= 2*(n * n) as u32 {

						println!{ "[[{},{}],[{},{}]], ", i % n as i32, i / n as i32, j % n as i32, j / n as i32}

						{
							let mut num = counter.lock().unwrap();
							*num += 1;
						}
						return 1;
					}

					{
						let num = counter.lock().unwrap();
						if *num > 0 {
							return 1;
						}
					}
				}

			}

			{
				let num = counter.lock().unwrap();
				if *num > 0 {
					return 1;
				}
			}
		}
	}

	return 0;
}
