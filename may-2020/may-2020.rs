extern crate rand;

use rand::Rng;
use std::fmt;
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

#[repr(u8)]
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum Cell {
    Dead = 0,
    Alive = 1,
}


#[derive(Hash)]
pub struct Universe {
    width: u32,
    height: u32,
    cells: Vec<Cell>,
    born: Vec<u8>,
    stay: Vec<u8>,
}


impl Universe {
    fn get_index(&self, row: u32, column: u32) -> usize {
        (row * self.width + column) as usize
    }

    fn live_neighbor_count(&self, row: u32, column: u32) -> u8 {
        let mut count = 0;
        for delta_row in [self.height - 1, 0, 1].iter().cloned() {
            for delta_col in [self.width - 1, 0, 1].iter().cloned() {
                if delta_row == 0 && delta_col == 0 {
                    continue;
                }
                if delta_row != 0 && delta_col != 0 {
                    continue;
                }

                let neighbor_row = (row + delta_row) % self.height;
                let neighbor_col = (column + delta_col) % self.width;
                let idx = self.get_index(neighbor_row, neighbor_col);
                count += self.cells[idx] as u8;
            }
        }
        count
    }
}



fn random_born() -> Vec<u8> {
    let mut rng = rand::thread_rng();

    let mut alive = Vec::new();

    if rng.gen::<f64>() < 0.5 {
        alive.push(0 as u8);
    }

    if rng.gen::<f64>() < 0.5 {
        alive.push(1 as u8);
    }

    if rng.gen::<f64>() < 0.5 {
        alive.push(2 as u8);
    }

    if rng.gen::<f64>() < 0.5 {
        alive.push(3 as u8);
    }

    if rng.gen::<f64>() < 0.9 {
        alive.push(4 as u8);
    }

    alive
}

fn random_live() -> Vec<u8> {
    let mut rng = rand::thread_rng();

    let mut alive = Vec::new();

    if rng.gen::<f64>() < 0.5 {
        alive.push(0 as u8);
    }

    if rng.gen::<f64>() < 0.5 {
        alive.push(1 as u8);
    }

    if rng.gen::<f64>() < 0.5 {
        alive.push(2 as u8);
    }

    if rng.gen::<f64>() < 0.5 {
        alive.push(3 as u8);
    }

    if rng.gen::<f64>() < 0.9 {
        alive.push(4 as u8);
    }

    alive
}



impl Universe {
    pub fn tick(&mut self) {
        let mut next = self.cells.clone();


        for row in 0..self.height {
            for col in 0..self.width {
                let idx = self.get_index(row, col);
                let cell = self.cells[idx];
                let live_neighbors = self.live_neighbor_count(row, col);


                let next_cell = match (cell, live_neighbors) {
                    // Rule 1: Any live cell with fewer than two live neighbours
                    // dies, as if caused by underpopulation.
                    (Cell::Alive, x) if self.stay.contains(&x) => Cell::Alive,
                    // Rule 2: Any live cell with two or three live neighbours
                    // lives on to the next generation.
                    (Cell::Dead, y) if self.born.contains(&y) => Cell::Alive,
                    // Rule 3: Any live cell with more than three live
                    // neighbours dies, as if by overpopulation.
                    (_otherwise, _) => Cell::Dead,
                };

                next[idx] = next_cell;
            }
        }

        self.cells = next;
    }

    pub fn render(&self) -> String {
        self.to_string()
    }

    pub fn width(&self) -> u32 {
        self.width
    }

    pub fn height(&self) -> u32 {
        self.height
    }

    pub fn cells(&self) -> *const Cell {
        self.cells.as_ptr()
    }

}


impl fmt::Display for Universe {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for line in self.cells.as_slice().chunks(self.width as usize) {
            for &cell in line {
                let symbol = if cell == Cell::Dead { '◻' } else { '◼' };
                write!(f, "{}", symbol)?;
            }
            write!(f, "\n")?;
        }

        Ok(())
    }
}

fn random_cell(chance: u16) -> Cell {
    let mut rng = rand::thread_rng();

    let random_int: u16 = rng.gen();

    let mut random_cell = Cell::Dead;

    // 2**16 = 65536
    if random_int < chance {
        random_cell = Cell::Alive;
    }

    random_cell
}

pub fn new_uni(c: u16) -> Universe {
        let width = 11;
        let height = 11;

        let cells = vec![random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), 
                            random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), 
                            random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), 
                            random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), 
                            random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), 
                            random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), Cell::Alive, random_cell(c), random_cell(c), random_cell(c), 
                            random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), 
                            random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), 
                            random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), 
                            random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), 
                            random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), random_cell(c), ];

        let born = random_born();
        let stay = random_live();

        Universe {
            width,
            height,
            cells,
            born,
            stay,
        }
    }

fn calculate_hash<T: Hash>(t: &T) -> u64 {
    let mut s = DefaultHasher::new();
    t.hash(&mut s);
    s.finish()
}

fn check_loops(hashes: &[u64]) -> i32 {

    let first_hash = hashes[0];

    let mut first = 1;
    let mut cycle_count = 0;

    let mut cycle = 0;

    for hash in hashes.iter() {
        if first != 1{
            if *hash == first_hash {
                cycle = cycle_count;
                break;
            }
        }
        cycle_count += 1;

        first = 0;
    }

    cycle
}

fn main() {
    

    println!("Trying to find life...");
    let _cells = vec![ Cell::Alive ];


    const CYCLE_CHECK: usize = 10000;
    const INIT_PERIOD: usize = 10000000;
    const UNIVERSE_LIFE: usize = CYCLE_CHECK + INIT_PERIOD;

    let mut max_cycle = 0;
    let mut error = 72;
    let mut best = 0;

    let mut init_i = 0;
    let mut check_i = 0;

    let mut early_cycle = 0;

    let check_each = 50000;

    loop {
        let mut chance :u16 = 600;

        let mut rng = rand::thread_rng();

        if rng.gen::<f64>() < 0.2 {
            chance = 1000;
        }
                if rng.gen::<f64>() < 0.2 {
            chance = 2000;
        }

        if rng.gen::<f64>() < 0.2 {
            chance = 4000;
        }
        
        if rng.gen::<f64>() < 0.2 {
            chance = 8000;
        }

        if rng.gen::<f64>() < 0.2 {
            chance = 16000;
        }

        if rng.gen::<f64>() < 0.2 {
            chance = 32000;
        }


        print!("---\nUniverse Chance: {:?}\n", chance);

        let mut uni: Universe = new_uni(chance);
        print!("Universe Born: {:?}\n", uni.born);
        print!("Universe Stay: {:?}\n", uni.stay);

        
        let mut universe_hashes: [u64; CYCLE_CHECK] = [0; CYCLE_CHECK];
        let mut init_universe_hashes: [u64; 1001] = [0; 1001];
        let mut universe_cycle = 0;

        print!( "Start-of-Universe: {:?}\n", uni.cells);

        early_cycle = 0;

        init_i = 0;
        for n in 1..UNIVERSE_LIFE {

            uni.tick();


            if n >= 100000 && n <= 101000 {
                init_universe_hashes[init_i] = calculate_hash(&uni);
                init_i += 1;
            }

            if n == 101001 {
                let universe_init_cycle = check_loops( &init_universe_hashes);
                //print!("init cycle: {}\n", universe_init_cycle);
                if universe_init_cycle > 0 {
                    early_cycle = 1;
                    //break;
                }
            }

            if n % check_each < CYCLE_CHECK {
                check_i = n % check_each;
                universe_hashes[check_i] = calculate_hash(&uni);
            }
            if n % check_each == CYCLE_CHECK {
                universe_cycle = check_loops( &universe_hashes);
                if universe_cycle > 0 {
                    //print!("cycle: {}, n: {}\n", universe_cycle, n);

                    if n < 100000 {
                        early_cycle = 1;
                    }
                    //break;
                }


            }
   
        }

        if early_cycle == 0 {

            let diff = universe_cycle - 72;
            if diff.abs() < error {
                error = diff.abs();
                best = universe_cycle;
            }
            if universe_cycle > max_cycle {
                max_cycle = universe_cycle;
            }

            //print!("{}\n",uni);
        }
        print!("Universe Cycle: {}, best: {}, max: {}\n", universe_cycle, best, max_cycle);

        
        // if universe_cycle == 72 {
        //     print!("We found 72-cycled life!");
        //     println!("{:?}\n", uni.cells);
        //     break;
        // }

    }
}
