# Ponder This

Solution Attempts for Ponder This Challenges

> As to methods there may be a million and then some, but principles are few. The man who grasps principles can successfully select his own methods. The man who tries methods, ignoring principles, is sure to have trouble.

*HARRINGTON EMERSON, RENOWNED EFFICIENCY ENGINEER AND BUSINESS THEORIST*

